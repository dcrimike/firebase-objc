//
//  AppDelegate.h
//  FBobjc
//
//  Created by Mike Revoir on 7/14/16.
//  Copyright © 2016 Duke Institute for Health Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

