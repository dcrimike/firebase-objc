//
//  main.m
//  FBobjc
//
//  Created by Mike Revoir on 7/14/16.
//  Copyright © 2016 Duke Institute for Health Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
